#!/usr/bin/env python
#-*- coding:utf-8 -*-

import pandas as pd
from anytree.node import Node
from anytree.exporter import DotExporter

# from anytree.exporter import DotExporter

df = pd.read_csv("windows_col.csv", header=None)
nodes = {}
i = 1


while i < 402:
    process_depth = int(df.loc[i, 0])
    pid = int(df.loc[i, 1])
    ppid = int(df.loc[i, 2])
    process_name = str(df.loc[i, 3])
    process_info = "{0}\nPID:{1}".format(process_name, pid)
    if process_depth == 0:
        node = Node(process_name)
    else:
        node = Node(process_name, nodes[ppid])
    nodes[pid] = node
    i += 1

DotExporter(nodes[next(iter(nodes))]).to_picture("tree.png")


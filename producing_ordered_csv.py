#!/usr/bin/env python
#-*- coding:utf-8 -*-

import openpyxl as op
import pandas as pd

df = pd.read_csv("windows.csv", header=None)
df = df.iloc[:, 0:4]
df.drop_duplicates()
df.to_csv("windows_col.csv", index=False, header=False)
